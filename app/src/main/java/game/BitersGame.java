package game;

/**
 * Classe che estende AndroidGame e rappresenta la main activity
 *
 *
 */

import android.os.Bundle;
import android.util.SparseArray;

import com.badlogic.androidgames.framework.Screen;
import com.badlogic.androidgames.framework.impl.AndroidGame;

import game.screen.LoadingScreen;

public class BitersGame extends AndroidGame{
    public Level[] levels; // array che contiene i livelli
    public SparseArray<Screen> screens; // array che contiene gli Screen
    public int status = 0; // stato del gioco che può assumere i valori definiti sotto

    public static final int RUNNING = 0; // stato di running
    public static final int PAUSE = 1; // stato di pausa
    public static final int GAMEOVER = 2; // stato di game over
    public static final int WIN = 3; // stato di vittoria



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public Screen getStartScreen() {
        /*
        il primo screen che richiamo è il LoadingScreen
        che si occupa di caricare gli oggetti di gioco e di visualizzare
        una schermata di loading
         */
        return new LoadingScreen(this);

    }
}
