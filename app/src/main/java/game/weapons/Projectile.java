package game.weapons;

import com.badlogic.androidgames.framework.Pixmap;

import game.Asset;
import game.enemy.Enemy;

/**
 * Classe che generalizza un proiettile, definendone le caratteristiche di base
 *
 *
 */

public abstract class Projectile {
    public int x;
    public int y;
    protected int direction;
    public boolean active = false;
    public int damage = 25;
    public Pixmap pixmap = Asset.simpleProjectile;


    /**
     * metodo che implementa lo sparo del proiettile
     */
    public abstract void fireProjectile();


    /**
     * metodo che implementa l'aggiornamento dello stato del proiettile nel tempo
     * @param deltaTime
     * @param windowWidth
     */
    public abstract void update(float deltaTime, int windowWidth);


    /**
     * metodo che controlla la collisione del proiettile con un nemico
     * @param z
     * @return
     */
    public abstract boolean checkCollision(Enemy z);

}
