package game.weapons;

import android.util.SparseArray;

import com.badlogic.androidgames.framework.Pool;

/**
 * Classe che generalizza la creazione di armi per il player
 *
 *
 */

public abstract class Weapon {
    public Pool<Projectile> projectilePool; //pool di proiettili
    public boolean switchY; // distanziatore verticale per evitare la sovrapposizione

    /**
     * metodo che implementa lo sparo dell'arma
     * @param startX x iniziale
     * @param startY y iniziale
     * @param direction direzione dello sparo
     * @param activeProjectiles proiettili attivi
     */
    public abstract void fire(int startX, int startY, int direction, SparseArray<Projectile> activeProjectiles);



    /**
     * metodo che implementa il cambio di munizioni
     * @param pool
     */
    public abstract void changeProjectile(Pool<Projectile> pool);



    /**
     * ripristino alle condizioni iniziali
     */
    public abstract void reset();

}
