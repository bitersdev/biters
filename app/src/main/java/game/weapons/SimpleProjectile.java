package game.weapons;

import game.enemy.Enemy;
import game.interfaces.Walker;

/**
 * Classe che implementa un proiettile di base
 *
 *
 */

public class SimpleProjectile extends Projectile{

    private float stepTime = 0;
    private static final float timeBeforeStep = 0.01f;


    public SimpleProjectile() {}

    @Override
    public void fireProjectile(){
        // inizializza
        if(this.active) return;
        this.active = true;

    }

    public void update(float deltaTime, int windowWidth) {

        if(!active) return;

        //se il proiettile esce dalla finestra visibile allora lo disabilito
        if(this.x > windowWidth || this.x <= -26) {
            disableProjectile();
            return;
        }
        stepTime += deltaTime;

        //questo while garantisce che lo spostamento del proiettile dipenda solo dal tempo
        while (stepTime > timeBeforeStep) {
            stepTime -= timeBeforeStep;
            if(direction == Walker.DIR_RIGHT) {
                    this.x += 2;
            }
            else {
                    this.x -= 2;
            }
        }
    }


    /**
     * metodo che disabilita un proiettile
     */
    private void disableProjectile() {
        this.active = false;
        this.stepTime = 0;
        this.x = 0;
        this.y = 0;
    }



    public boolean checkCollision(Enemy z) {
        //se il proiettile collide con un nemico vivo  lo disabilito
        if(z.isAlive() && z.collided(this.x + 13, this.y + 10)){

            this.disableProjectile();
            return true;
        }
        else{
            return false;
        }
    }
}
