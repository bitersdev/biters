package game.weapons;

import android.util.SparseArray;

import com.badlogic.androidgames.framework.Pool;

import java.util.List;

import game.Asset;
import game.Settings;

/**
 * Classe che definiscie un'arma di base per il player
 *
 *
 */

public class BasicWeapon  extends Weapon{

    /**
     * costruttore chhe istanzia una poll di SimpleProjectile
     */
    public BasicWeapon() {
        Pool.PoolObjectFactory<Projectile> poolFactory = new Pool.PoolObjectFactory<Projectile>() {
            @Override
            public Projectile createObject() {
                return new SimpleProjectile();
            }
        };
        this.projectilePool = new Pool<>(poolFactory, 10);
    }

    @Override
    public void fire(int x , int y, int direction , SparseArray<Projectile> activeProjectiles) {
        Projectile p = this.projectilePool.newObject();
        if(!switchY){
            this.switchY = true;
            p.y = y + 5;
        }else{
            this.switchY = false;
            p.y = y + 10;
        }

        p.x = x;
        p.direction = direction;
        p.fireProjectile();

        if (Settings.effectsEnabled)
            Asset.fire.play(1);
        activeProjectiles.append(p.hashCode(),p);
    }


    @Override
    public void changeProjectile(Pool<Projectile> pool){
        this.projectilePool = pool;
    }


    @Override
    public void reset(){
        Pool.PoolObjectFactory<Projectile> poolFactory = new Pool.PoolObjectFactory<Projectile>() {
            @Override
            public Projectile createObject() {
                return new SimpleProjectile();
            }
        };
        this.projectilePool = new Pool<>(poolFactory, 10);
    }
}
