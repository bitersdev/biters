package game.enemy;

import game.interfaces.Walker;
import game.pattern.Entity;

/**
 * Classe astratta che fornisce una base per la creazione di un
 * qualsiasi nemico all'interno del gioco
 *
 *
 */

public abstract class Enemy extends Entity  implements Walker {
    public static final int DEAD = 3;

    public int reward = 1; // punti attribuiti al player in caso di sconfitta del nemico
    public int relativeX = 0; // posizione relativa alla finestra visualizzata
    public int relativeY = 0; // posizione relativa alla finestra visualizzata
    public int absoluteX = 0; // posizione assoluta in tutta la schermata di gioco
    public int absoluteY = 0; // posizione assoluta in tutta la schermata di gioco

    // posizione iniziale assoluta
    public int startX = 0;
    public int startY = 0;

    public int status = 0; // stato del nemico
    public int lifePoints; // punti vita del nemico
    public int startLifePoints; // punti vita iniziali del nemico utili epr il ripristino
    public int damage; // danno provocato dal nemico


    /**
     * metodo per gestire la collisione col nemico
     * restituisce true se c'è collisione, false altrimenti
     *
     * @param x
     * @param y
     * @return
     */
    public abstract boolean collided(int x, int y);


    /**
     * ripristina le condizioni iniziali
     */
    public void reset(){
        this.relativeX = this.startX;
        this.relativeY = this.startY;
        this.absoluteX = this.startX;
        this.absoluteY = this.startY;
        this.status = 0;
        this.lifePoints = this.startLifePoints;
    }


    /**
     * metodo che verifica lo stato del nemico
     * restituisce true se è vivo, false altrimenti
     * @return
     */
    public boolean isAlive(){
        if(this.status == Enemy.DEAD)
            return false;
        else
            return true;

    }

}
