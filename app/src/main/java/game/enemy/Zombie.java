package game.enemy;

import com.badlogic.androidgames.framework.Game;

import game.component.AIComponent;
import game.component.GraphicComponent;
import game.interfaces.Walker;
import util.Constant;

/**
 * Classe che definisce il nemico di tipo zombie
 *
 */

public class Zombie extends Enemy {

    public int lastDirection;
    public float stepTime = 0;
    private float animationTime = 0;
    private int step = 1;
    private int row;
    private boolean verso = false;
    private static final float timeBeforeStep = 0.10f;
    private static final float timeBeforeAnimation = 0.2f;


    /**
     *  Constructor for zombie
     * @param graphic
     * @param aiComponent
     */
    public Zombie(GraphicComponent graphic, AIComponent aiComponent){
        this.addComponent(Constant.GRAPHIC_COMPONENT, graphic);
        this.addComponent(Constant.AI_COMPONENT, aiComponent);
    }



    @Override
    public void updateWalk(int direction, float deltaTime, boolean backgroundScrolling) {
        this.status = Walker.WALK;
        this.stepTime += deltaTime;

        // se la direzione è cambiata bisogna ridisegnare subito quindi modifico il valore di animationTime
        if(direction != this.lastDirection){
            this.animationTime = 1;
            this.lastDirection = direction;
        }

        //questo while garantisce che la posizione del player dipenda solo da tempo
        while (stepTime > timeBeforeStep) {
            stepTime -= timeBeforeStep;
            if(direction == Walker.DIR_RIGHT) {
                this.relativeX += 3;
                this.absoluteX += 3;
            }
            else if(direction == Walker.DIR_LEFT) {
                this.relativeX -= 3;
                this.absoluteX -= 3;
            }
        }
    }

    @Override
    public void presentWalk(Game game, float delay) {
        GraphicComponent comp = (GraphicComponent) this.getComponent(Constant.GRAPHIC_COMPONENT);
        animationTime += delay; // somma del tempo trascorso

        //se il tempo trascrso è abbastanza eseguo il prossimo passo di animazione
        if(animationTime > timeBeforeAnimation) {
            animationTime = 0;

            //selziono la riga in base alla direzione
            if(this.lastDirection == Walker.DIR_RIGHT) {
                row = 2;
            } else{
                row = 1;
            }

            //seleziono la colonna in base all'ultima colonna selezionata
            if(step < 0){
                step = 0;
            }
            if (step == 2)
                verso = false;
            else if (step == 0)
                verso = true;

             /*
            siccoome il camminamento è composto da 3 finestre in successione
            prima in un verso e poi nell'altro, uso la variabile verso per definire
            lo spostamento di colonna all'interno della spritesheet
             */
            if (verso)
                step++;
            else
                step--;
            game.getGraphics().drawPixmap(comp.image, this.relativeX, this.relativeY, comp.windowWidth * step, comp.windowHeight * row, comp.windowWidth, comp.windowHeight);

        }
        //se non ho raggiunto il tempo per il passo di animazione successivo allora disegno il passo attuale
        else{

            game.getGraphics().drawPixmap(comp.image, this.relativeX, this.relativeY, comp.windowWidth * step, comp.windowHeight * row, comp.windowWidth, comp.windowHeight);
        }
    }

    @Override
    public void updateStand( float delay) {
        this.status = Walker.STAND;
    }

    @Override
    public void presentStand(Game game, float delay) {
        GraphicComponent comp = (GraphicComponent) this.getComponent(Constant.GRAPHIC_COMPONENT);
        if(this.lastDirection == Walker.DIR_RIGHT)
            game.getGraphics().drawPixmap(comp.image, this.relativeX, this.relativeY, comp.windowWidth * 1, comp.windowHeight * 2, comp.windowWidth, comp.windowHeight);
        else
            game.getGraphics().drawPixmap(comp.image, this.relativeX, this.relativeY, comp.windowWidth * 1, comp.windowHeight * 1 , comp.windowWidth, comp.windowHeight);

    }


    @Override
    public boolean collided(int x, int y) {
        GraphicComponent zComp = (GraphicComponent) this.getComponent(Constant.GRAPHIC_COMPONENT);
        if (x >= this.relativeX && x <= this.relativeX + zComp.windowWidth && y >= this.absoluteY +32 && y <= this.absoluteY + zComp.windowHeight)
            return true;
        else
            return false;
    }
}

