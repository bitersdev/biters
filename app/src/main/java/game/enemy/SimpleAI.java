package game.enemy;

import game.Player;
import game.World;
import game.component.AIComponent;
import game.component.GraphicComponent;
import game.interfaces.Walker;
import util.Constant;

/**
 * Classe che definisce un'intelligenza artificiale semplice
 * basata su un albero di decisione
 *
 *
 */

public class SimpleAI extends AIComponent{

    @Override
    public void makeDecision(World world, float deltaTime){
        Player player = world.player;
        Enemy zombie = (Enemy) this.owner;

        //recupero le componenti grafiche
        GraphicComponent playerComponent = (GraphicComponent) player.getComponent(Constant.GRAPHIC_COMPONENT);
        GraphicComponent zombieComponent = (GraphicComponent) zombie.getComponent(Constant.GRAPHIC_COMPONENT);

        //controllo se il nemico si trova nella finestra visible altrimenti lo lascio fermo
        if( (zombie.absoluteX - 480) <= world.background.currentX) {
            //verifico se il nemico si trova a destra o a sinistra rispetto al player
            if (player.absoluteX + (playerComponent.windowWidth/2) < zombie.absoluteX) {
                //il nemico è a destra
                zombie.updateWalk(Walker.DIR_LEFT, deltaTime, world.backgroundScrolling);
            } else if (player.absoluteX + (playerComponent.windowWidth/2) > zombie.absoluteX + zombieComponent.windowWidth){
                //il nemico è a sinistra
                zombie.updateWalk(Walker.DIR_RIGHT, deltaTime, world.backgroundScrolling);
            }
            else{
                zombie.updateStand(deltaTime);
            }
        }
    }
}
