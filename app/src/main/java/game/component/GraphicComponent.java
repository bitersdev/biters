package game.component;

import com.badlogic.androidgames.framework.Pixmap;

import game.pattern.Component;

/**
 * Classe che definisce la componente grafica di un'entità
 *
 *
 */
public class GraphicComponent extends Component{
    public Pixmap image;
    public int windowWidth;
    public int windowHeight;
    public int height;
    public int width;

    public GraphicComponent(Pixmap image, int windowWidth, int windowHeight, int width, int height) {
        this.windowWidth = windowWidth;
        this.image = image;
        this.windowHeight = windowHeight;
        this.height = height;
        this.width = width;
    }

    public GraphicComponent(Pixmap image, int windowWidth, int windowHeight) {
        this.image = image;
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
    }
}
