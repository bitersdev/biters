package game.component;

import com.badlogic.androidgames.framework.Sound;

import game.pattern.Component;

/**
 * classe che definisce la componente effetto sonoro di un'entità
 *
 *
 */

public class SoundComponent extends Component{
    public Sound sound;

    public SoundComponent(Sound sound) {
        this.sound = sound;
    }
}
