package game.component;

import com.badlogic.androidgames.framework.Music;

import game.pattern.Component;

/**
 * Classe che definisce la Componente musicale di un'entità
 *
 *
 */

public class MusicComponent extends Component {
    public Music music;

    public MusicComponent(Music music) {
        this.music = music;
    }
}
