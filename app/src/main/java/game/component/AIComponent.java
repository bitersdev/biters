package game.component;

import game.enemy.Enemy;
import game.Player;
import game.World;
import game.interfaces.Walker;
import game.pattern.Component;
import util.Constant;

/**
 * Classe astratta per definire la struttura di una
 * tipica intelligenza artificiale
 *
 */

public abstract class AIComponent extends Component {

    /**
     * metodo che determina l'azione da compiere da parte dell'intelligenza artificiale
     *
     * @param world modello del mondo
     * @param deltaTime tempo trascorso dall'ultimo frame
     */
    public abstract void makeDecision(World world, float deltaTime);
}
