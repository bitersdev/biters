package game;

import android.util.SparseArray;

import com.badlogic.androidgames.framework.Input;

import java.util.List;

import game.component.AIComponent;
import game.component.GraphicComponent;
import game.enemy.Boss;
import game.enemy.Enemy;
import game.enemy.Zombie;
import game.gameobject.acquirable.Acquirable;
import game.weapons.Projectile;
import util.Constant;

/**
 * classe che rappresenta il modello del mondo. Quindi è composta dal player,
 * dai nemici, dal boss, e da tutti gli oggetti presenti nel mondo
 *
 *
 */
public class World {
    public Player player; // player
    public Enemy[] enemies; // array di nemici
    public Boss[] bosses; // array di boss
    public Acquirable[] acquirables; // array di oggetti acquisibili(bonus/malus)
    public Background background; // sfondo
    public boolean backgroundScrolling = false; // indicatore per lo scorrimento dello sfondo

    public SparseArray<Projectile> activeProjectiles= new SparseArray<>(); // contenitore dei proiettili attivi


    /**
     * Costruttore del mondo
     *
     * @param player giocatore
     * @param background sfondo
     * @param enemies array di nemici
     * @param bosses array di boss
     */
    public World(Player player, Background background, Enemy[] enemies, Boss[] bosses) {
        this.player = player;
        this.enemies = enemies;
        this.background = background;
        this.bosses = bosses;
    }


    /**
     * matodo per l'update del modello del mondo
     *
     * @param events lista degli eventi touch
     * @param deltaTime tempo trascorso dall'ultimo frame
     */
    public void update(List<Input.TouchEvent> events, float deltaTime){

        // aggiorna i proiettili attivi e toglie dalla lista quelli che non sono più attivi
        updateProjectiles(deltaTime);

        //update background status
        updateBackground(player.absoluteX, player.relativeX );

        //update zombie status
        updateEnemy(deltaTime, enemies);

        //update acquirables
        updateAcquirables(player);

    }


    /**
     * metodo per l'update dello stato dei proiettili
     *
     * @param deltaTime tempo trascorso dal'ultimo frame
     */
    private void updateProjectiles(float deltaTime) {
        Projectile projectile;
        //scorro l'array dei proiettili attivi
        for (int i = 0; i < activeProjectiles.size(); i++) {
            projectile = activeProjectiles.valueAt(i);

            //eseguo l'update dello stato del proiettile
            projectile.update(deltaTime,this.background.windowWidth);

            //se dopo l'update il proiettile risulta inattivo(potrebbe essere uscito dalla finestra visibile)
            // allora lo cancello dai proiettili attivi e lo rendo disponibile nella pool di proiettili
            if(!projectile.active) {
                activeProjectiles.delete(projectile.hashCode());
                if(projectile.getClass().equals( player.getWeapon().projectilePool.newObject().getClass())){
                    player.getWeapon().projectilePool.free(projectile);
                }
            }else{// se il proiettile è attivo controllo le collisioni coi nemici
                for (Enemy e : enemies) {
                    if(projectile.checkCollision(e)){
                        //se il proiettile ha colpito un nemico aggiorno i punti vita del nemico
                        e.lifePoints -= projectile.damage;
                        if(e.lifePoints <= 0 ){
                            e.status = Enemy.DEAD;
                            this.player.score += e.reward;
                        }
                    }
                }
            }
        }
    }


    /**
     * effettua l'update dello stato dei nemici
     *
     * @param deltaTime tempo trascorso dall'ultimo frame
     * @param zombies array di nemici
     */
    private void updateEnemy(float deltaTime, Enemy[] zombies) {
        AIComponent ai;
        for(int i = 0; i < zombies.length; i++){
            //per ogni nemico consulto l'intelligenza artificiale per scegliere quale azione compiere
            if(zombies[i].status != Zombie.DEAD) {
                ai = (AIComponent) zombies[i].getComponent(Constant.AI_COMPONENT);
                ai.makeDecision(this, deltaTime);
                // controllo se il nemico ha colpito il player
                player.checkBitten(zombies[i]);
            }
        }
    }


    /**
     * aggiornamento dello stato del background.
     *
     * @param playerAbsoluteX x assoluta del player
     * @param playerRelativeX x relativa del player
     */
    private void updateBackground(int playerAbsoluteX, int playerRelativeX) {
        int avanzamento = playerRelativeX - 240; //  per capire se il player ha superato la metà dello sfondo visibile
        int avanzamentoAssoluto = playerAbsoluteX - 240; // rappresenta la nuova x da cui partire per disegnare lo sfondo
        int avanzamentoBackground = 0;
        GraphicComponent image = (GraphicComponent) background.getComponent(Constant.GRAPHIC_COMPONENT);

        //se il player ha superato la metà dello sfondo visibile e lo sfondo non è terminato faccio scorrere il background
        if(avanzamento > 0 && player.lastDirection == Player.DIR_RIGHT && !background.ended() ){
            backgroundScrolling = true;
            avanzamentoBackground = avanzamentoAssoluto - background.currentX; //di quanto mi sono spostato dall'ultimo scorrimento
            background.currentX = avanzamentoAssoluto; // imposto la nuova x iniziale dello sfondo

            // update delle coordinate degli zombie se lo schermo scorre
            for(Enemy z : this.enemies){
                z.relativeX -= avanzamentoBackground;
            }

            //update delle coordinate dei proiettili se lo schermo scorre
            Projectile projectile;
            for (int i = 0; i < activeProjectiles.size(); i++) {
                projectile = activeProjectiles.valueAt(i);
                projectile.x -= avanzamentoBackground;
            }

            //update delle coordinate degli acquirable
            for(Acquirable a : this.acquirables){
                a.x -= avanzamentoBackground;
            }
        }
        else{
            if(background.ended()){
                background.currentX = image.width - 480;
            }
            backgroundScrolling = false;
        }
    }


    /**
     * aggiorna lo stato deo oggetti acquisibili
     *
     * @param player
     */
    private void updateAcquirables(Player player){
        //se sono stati acquisiti  richiamo il metdodo acquire per scatenarne l'effetto
        for(Acquirable a : acquirables){
            if(a.collided(player.relativeX + 16, player.relativeY +32)){
                a.acquired = true;
                a.acquire(this, player);
            }
        }
    }


    /**
     * ripristino alle condizioni iniziali
     */
    public void reset() {
        for(Enemy e : enemies){
            e.reset();
        }
        player.reset();
        background.reset();

        for(Acquirable a : this.acquirables){
            a.reset();
        }
        //reset dei proiettili
        activeProjectiles = new SparseArray<>();
    }
}
