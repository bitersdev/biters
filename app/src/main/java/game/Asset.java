package game;

import com.badlogic.androidgames.framework.Music;
import com.badlogic.androidgames.framework.Pixmap;
import com.badlogic.androidgames.framework.Sound;

/**
 * Classe contenente tutti  gli asset utilizzati: immagini, sfondi, spritesheet, autdio ecc...
 *
 *
 */
public class Asset {

    //-------------------Spritesheet--------------------------------------------
    public static Pixmap player; // spritesheet del player
    public static Pixmap zombie; // spritesheet



    //-------------------Asset relativi ai menu---------------------------------
    public static Pixmap loading; // immagine di caricamento
    public static Pixmap startBackground; //immagine di sfondo generico per le schermate dei menu
    public static Pixmap logo; // immagine con la scritta "Biters' utilizzata nei menu
    public static Pixmap zombieMenu; // immagine di uno zombie utilizzata nel main menu
    public static Pixmap newGame; // tasto new game
    public static Pixmap loadGame; // tasto load game
    public static Pixmap options; // tasto option
    public static Pixmap credits; // tasto credits
    public static Pixmap level1; // Riquadro per la scelta del primo livello
    public static Pixmap level2; // riquadro per la scelta del secondo livello
    public static Pixmap level3; // riquadro per la scelta del terzo livello
    public static Sound click; // suono emesso alla pressione di un tasto del menu
    public static Music menu_soundtrack; // suono di sottofondo del menu
    public static Pixmap on_sound; // immagine del tasto on  per la musica di sottofondo
    public static Pixmap off_sound; // immagine del tasto off per la musica di sottofondo
    public static Pixmap on_effects; // immagine del tasto on per gli effetti sonori
    public static Pixmap off_effects; // immagine del tasto on per gli effetti sonori
    public static Pixmap sound; // label della musica di sottofondo nel menu
    public static Pixmap return_button; // immagine del tasto indietro ustao nei menu
    public static Pixmap options_menu; // label Options presente nel menu options
    public static Pixmap effects; // label degli effetti sonori nel menu
    public static Pixmap zombieOptions; // immagine di zombie utilizzata nel menu delle opzioni
    public static Pixmap credits_menu; // label Credits presente nella schermata credits
    public static Pixmap zombie_tv; // immagine di uno zombie presente nella schermata credits




    //----------------Assett relativi alla fase di gioco-----------------------
    public static Pixmap background; // sfondo del primo livello
    public static Pixmap buttonLeft; // immagine del tasto per lo spostamento del player a sinistra
    public static Pixmap buttonRight; // immagine del tasto per lo spostamento del player a destra
    public static Pixmap buttonFire; // immagine del tasto per far sparare il player
    public static Pixmap buttonJump; // immagine del tasto per far saltare il player
    public static Pixmap simpleProjectile; // immagine di un proiettile semplice
    public static Pixmap redProjectile; // immagine di un proiettile rosso
    public static Pixmap scoreIcon; // icona che indica il puteggio
    public static Pixmap numbers; // immagine contenente i numeri da 1 a 9
    public static Sound fire; // suono relativo allo sparo
    public static Music soundtrack; // sottofondo relativo alla fase di gioco
    public static Pixmap transparentBackground; // sfondo semitrasparente per i menu in fase di gioco
    public static Pixmap inGamePauseButton; // immagine del tasto pausa in game
    public static Pixmap gameover; // scritta GameOver
    public static Pixmap youWinText; // Scritta You Win
    public static Pixmap mainMenuBtn; // immagine del tasto per tornare al menu principale
    public static Pixmap tryAgainBtn; // immagine del tasto per riprovare il livello appena giocato
    public static Pixmap acquireRedProjectile; // immagine delle munizioni rosse

}
