package game;

import game.component.GraphicComponent;
import game.component.MusicComponent;
import game.pattern.Entity;
import util.Constant;

/**
 * Classe che rappresenta lo sfondo del gioco. Definisce l'immagine di sfondo
 * e i margini dello sfondo. Inoltre tiene traccia delle porzioni di sfondo attualmente
 * visualizzate (si presume che lo sfondo possa essere più largo della grandezza della superfice
 * visualizzabile a schermo)
 *
 *
 */


public class Background extends Entity{
    public int currentX; // x corrente
    public int currentY; // y corrente
    public final int windowWidth; // larghezza della porzione di sfondo da visualizzare
    public final int windowHeight; // alatezza della porzione di sfondo da visualizzare

    /**
     * Costruttore del Background
     *
     * @param image immagine di sfondo
     * @param soundtrack suono di sottofondo
     * @param currentX x iniziale
     * @param currentY y iniziale
     * @param windowWidth larghezza della porzione di sfondo da disegnare
     * @param windowHeight altezza della porzione di sfondo da disegnare
     */
    public Background(GraphicComponent image, MusicComponent soundtrack, int currentX, int currentY, int windowWidth, int windowHeight) {
        this.addComponent(Constant.GRAPHIC_COMPONENT, image);
        this.addComponent(Constant.MUSIC_COMPONENT, soundtrack);
        this.currentX = currentX;
        this.currentY = currentY;
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
    }


    /**
     * Metodo che restituisce true se è stata disegnata l'ultima porzione di sfondo
     * all'estrema destra. False altrimenti
     *
     * @return restituisce true se è stata disegnata l'ultima porzione di sfondo
     * all'estrema destra. False altrimenti
     */
    public boolean ended(){
        boolean result = false;
        GraphicComponent image = (GraphicComponent) this.getComponent(Constant.GRAPHIC_COMPONENT);
        if (currentX + image.windowWidth >= image.width){
            result = true;
        }
        return result;
    }

    /**
     * Metodo che riporta il background nella condizione iniziale
     */
    public void reset() {
        this.currentX = 0;
        this.currentY = 0;
    }
}
