package game;

import com.badlogic.androidgames.framework.Game;

import game.component.GraphicComponent;
import game.enemy.Enemy;
import game.interfaces.Jumper;
import game.interfaces.Walker;
import game.pattern.Entity;
import game.weapons.BasicWeapon;
import game.weapons.Weapon;
import util.Constant;

/**
 * Classe che rappresenta il player all'interno del gioco
 *
 *
 */
public class Player extends Entity implements Walker, Jumper{

    private Weapon weapon; // arma del player
    public int score = 0; // punteggio del player
    public int lifePoints = 100; // punti vita del player


    // posizione relativa
    public int relativeX = 0;
    public int relativeY = 0;

    //posizione assoluta
    public int absoluteX = 0;
    public int absoluteY = 0;

    public int lastDirection; // ultima direzionen

    //variabili di accumulo del tempo per update e present
    public float stepTime = 0;
    public float jumpStepTime = 0;
    private float animationTime = 0;

    //variabili necessarie alle animazioni e allo spostamento
    private int step = 6; //colonna della spritesheet
    private int row; //riga della spritesheet
    private boolean verso = false;
    public int status = 0;
    public int jumpStatus;
    private int jumpHeight = 100;  //altezza del salto in pixel
    public int startY = 0; // y al momento del salto
    private int jumpPhase = Jumper.ASCENDING;
    private static final float timeBeforeStep = 0.03f;
    private static final float timeBeforeAnimation = 0.2f;
    private static final float jumpTimeBeforeStep = 0.01f;


    public float lastAttackReceivedTime = 0; // variabile per garantire un tempo di invulnerabilità dopo un attacco ricevuto



    /**
     * Constructor for Player
     *
     * @param graphic GraphicComponent for the player avatar
     */
    public Player(GraphicComponent graphic){
        this.addComponent(Constant.GRAPHIC_COMPONENT, graphic);
        this.weapon = new BasicWeapon();
    }



    @Override
    public void updateWalk(int direction, float deltaTime, boolean backgroundScrolling) {
        this.status = Walker.WALK;
        this.stepTime += deltaTime;

        /*
        se la direzione è cambiata deve cambiare subito l'animazione senza
        aspettare il tempo di animazione quindi modifico animationTime
         */
        if(direction != this.lastDirection){
            this.animationTime = 1;
            this.lastDirection = direction;
        }

        //questo while garantisce che lo spostamento del player dipenda solo dal tempo
        while (stepTime > timeBeforeStep) {
            stepTime -= timeBeforeStep;
            if(direction == Walker.DIR_RIGHT) {
                // se il background sta scorrendo aggiorno solo la posizione relativa
                if(!backgroundScrolling)
                    this.relativeX += 4;
                this.absoluteX += 4;
            }
            else if(direction == Walker.DIR_LEFT) {
                //qui il controllo sul background non c'è perchè può scorrere solo andando a destra
                this.relativeX -= 4;
                this.absoluteX -= 4;
            }
        }
    }

    @Override
    public void presentWalk(Game game, float delay) {
        GraphicComponent comp = (GraphicComponent) this.getComponent(Constant.GRAPHIC_COMPONENT);
        animationTime += delay;
        if(animationTime > timeBeforeAnimation) {
            animationTime = 0;

            //in base alla direzione seleziono la riga
            if(this.lastDirection== Walker.DIR_RIGHT) {
                row = 0;

                // in base alla colonna precedente seleziono la successiva
                if(step < 6){
                    step = 6;
                }
                if (step == 8)
                    verso = false;
                else if (step == 6)
                    verso = true;
                if (verso)
                    step++;
                else
                    step--;
            } else{
                row = 4;
                if(step > 3){
                    step = 3;
                }
                if (step == 3)
                    verso = false;
                else if (step == 1)
                    verso = true;
                if (verso)
                    step++;
                else
                    step--;
            }
            game.getGraphics().drawPixmap(comp.image, this.relativeX, this.relativeY, comp.windowWidth * step, comp.windowHeight * row, comp.windowWidth, comp.windowHeight);

        }
        else{
            game.getGraphics().drawPixmap(comp.image, this.relativeX, this.relativeY, comp.windowWidth * step, comp.windowHeight * row, comp.windowWidth, comp.windowHeight);
        }
    }

    @Override
    public void updateStand( float delay) {
        this.status = Walker.STAND;
    }

    @Override
    public void presentStand(Game game, float delay) {
        GraphicComponent comp = (GraphicComponent) this.getComponent(Constant.GRAPHIC_COMPONENT);

        if(this.lastDirection == Walker.DIR_RIGHT)
            game.getGraphics().drawPixmap(comp.image, this.relativeX, this.relativeY, comp.windowWidth * 5, 0, comp.windowWidth, comp.windowHeight);
        else
            game.getGraphics().drawPixmap(comp.image, this.relativeX, this.relativeY, comp.windowWidth * 4, comp.windowHeight * 4 , comp.windowWidth, comp.windowHeight);
    }



    @Override
    public void updateJump(float deltaTime) {
        this.jumpStatus = Jumper.JUMP;
        this.jumpStepTime += deltaTime;
        while (jumpStepTime > jumpTimeBeforeStep) {
            jumpStepTime -= jumpTimeBeforeStep;

            // se ho raggiunto l'altezza massima del salto allora passo alla fase discendente
            if(this.startY - this.absoluteY >= this.jumpHeight){
                this.jumpPhase = Jumper.DESCENDING;
            }
            //se ho completato il salto ripristino le condizioni iniziali
            else if(this.jumpPhase == Jumper.DESCENDING && this.startY - this.absoluteY  <= 0){
                this.jumpStatus = 0;
                this.jumpPhase = Jumper.ASCENDING;
            }


            //se sono in fase di salto e ascendente decremento la y
            if(this.jumpStatus == Jumper.JUMP && this.startY - this.absoluteY <= jumpHeight && jumpPhase == Jumper.ASCENDING) {
                this.absoluteY -= 2;
                this.relativeY -= 2;
            }
            //se sono in fase di salto e discendente incremento la y
            else if(this.jumpStatus == Jumper.JUMP && this.jumpPhase == Jumper.DESCENDING){
                this.absoluteY += 2;
                this.relativeY += 2;
            }
        }
    }



    @Override
    public void presentJump(Game game, float delay) {
        GraphicComponent comp = (GraphicComponent) this.getComponent(Constant.GRAPHIC_COMPONENT);
        int step;
        int row;
        if(this.lastDirection== Walker.DIR_RIGHT) {
            step = 9;
            row = 0;
        } else{
            step = 0;
            row = 4;
        }
        game.getGraphics().drawPixmap(comp.image, this.relativeX, this.relativeY, comp.windowWidth * step, comp.windowHeight * row, comp.windowWidth, comp.windowHeight);
    }


    /**
     * ripristino alle condizioni iniziali
     */
    public void reset(){
        this.lifePoints = 100;
        this.absoluteX = 0;
        this.relativeX = 0;
        this.absoluteY = 210;
        this.relativeY = 210;

        this.lifePoints = 100;
        this.score = 0;

        this.jumpStatus = 0;
        this.jumpPhase = Jumper.ASCENDING;
        this.getWeapon().reset();

    }


    /**
     * metodo che controlla se il player è stato colpito da un nemico
     * in caso affermativo sottrae il danno ricevuto ai punti vita
     *
     * @param zombie nemico per cui effettuare il controllo
     */
    public void checkBitten(Enemy zombie) {
        float timeElapsed = (System.nanoTime()- this.lastAttackReceivedTime) / 1000000000.0f;
        GraphicComponent zComp = (GraphicComponent) zombie.getComponent(Constant.GRAPHIC_COMPONENT);
        int x = zombie.absoluteX + (zComp.windowWidth/2);
        int y = zombie.absoluteY;
        GraphicComponent pComp = (GraphicComponent) this.getComponent(Constant.GRAPHIC_COMPONENT);
        //controllo che il player non possa essere colpito più di una volta al secondo
        if(timeElapsed > 1) {
            if (x >= this.absoluteX && x <= this.absoluteX + pComp.windowWidth &&  (y + 32) <= this.absoluteY + pComp.windowHeight){
                this.lastAttackReceivedTime = System.nanoTime();
                this.lifePoints -= zombie.damage;
            }
        }
    }


    /**
     * Restituisce true se il player ha esaurito i punti vita, false altrimenti
     *
     * @return
     */
    public boolean isDead(){
        if (this.lifePoints <= 0) return true;
        else return false;
    }


    /**
     * restituisce l'arma del player
     * @return
     */
    public Weapon getWeapon() {
        return weapon;
    }

    /**
     * equipaggia il player con un'arma
     * @param weapon arma da equipaggiare
     */
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }
}
