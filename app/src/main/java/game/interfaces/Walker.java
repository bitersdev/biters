package game.interfaces;

import com.badlogic.androidgames.framework.Game;

/**
 * Interfaccia Walker: deve essere implementata da oggetti che hanno la capacità di camminare
 *
 *
 */
public interface Walker {
    int DIR_LEFT = 0;
    int DIR_RIGHT = 1;
    int WALK = 1;
    int STAND = 0;


    /**
     *  update delo stato in fase di camminata
     * @param direction
     * @param delay
     * @param backgroundScrolling
     */
    void updateWalk(int direction, float delay, boolean backgroundScrolling);


    /**
     * present dello  stato in fase di camminata
     * @param game
     * @param delay
     */
    void presentWalk(Game game, float delay);


    /**
     * update in caso di stazionamento
     *
     * @param delay
     */
    void updateStand( float delay);

    /**
     * present in casi di stazionamento
     *
     * @param game
     * @param delay
     */
    void presentStand(Game game, float delay);

}
