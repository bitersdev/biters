package game.interfaces;

import com.badlogic.androidgames.framework.Game;

/**
 * Interfaccia jumper: deve essere implementata da oggetti che
 * hanno la capacità di saltare
 *
 *
 */

public interface Jumper {
    public static final int JUMP = 3;
    public static final int ASCENDING = 4;
    public static final int DESCENDING = 5;

    /**
     * update dello  stato del salto
     * @param delay
     */
    void updateJump(float delay);


    /**
     * present dello stato del salto
     *
     * @param game
     * @param delay
     */
    void presentJump(Game game, float delay);
}
