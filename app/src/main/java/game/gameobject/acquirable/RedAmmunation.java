package game.gameobject.acquirable;

import com.badlogic.androidgames.framework.Pool;

import game.Asset;
import game.Player;
import game.World;
import game.component.GraphicComponent;
import game.weapons.Projectile;
import game.weapons.RedProjectile;
import game.weapons.SimpleProjectile;

/**
 * Classe che definisce un oggetto acquisibile che fornisce un nuovo tipo di munizioni
 * al player
 *
 *
 */

public class RedAmmunation extends Acquirable {


    public RedAmmunation(){
        this.graphic = new GraphicComponent(Asset.acquireRedProjectile,20,20);
    }

    /**
     * istanzia una nuova pool di RedProjectile e li assegna all'arma del player
     * @param world
     * @param player
     */
    @Override
    public void acquire(World world, Player player) {
        Pool.PoolObjectFactory<Projectile> poolFactory = new Pool.PoolObjectFactory<Projectile>() {
            @Override
            public Projectile createObject() {
                return new RedProjectile();
            }
        };
        Pool<Projectile> projectilePool = new Pool<>(poolFactory, 10);
        player.getWeapon().changeProjectile(projectilePool);
    }

    @Override
    public boolean collided(int x, int y) {
        if(x >= this.x && x <= (this.x + this.graphic.windowWidth) && y >= this.y ){
            return true;
        }
        else
            return false;
    }
}
