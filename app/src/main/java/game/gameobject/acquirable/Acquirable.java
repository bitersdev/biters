package game.gameobject.acquirable;

import game.Player;
import game.World;
import game.component.GraphicComponent;

/**
 * Classe per la generalizzazione di oggetti acquisibili
 *
 */

public abstract class Acquirable {
    public GraphicComponent graphic;
    public int x;
    public int y;
    public int startX;
    public int startY;
    public boolean acquired = false;


    /**
     *  metodo per verificare la collisione con l'oggetto
     * @param x
     * @param y
     * @return
     */
    public abstract boolean collided(int x, int y);



    /**
     * metodo per definire gli effetti dell'acquisizione dell'oggetto
     *
     * @param world
     * @param player
     */
    public abstract void acquire(World world, Player player);



    /**
     * ripristino alle condizioni iniziali
     */
    public  void reset(){
        this.x = this.startX;
        this.y = this.startY;
        this.acquired = false;
    };

}
