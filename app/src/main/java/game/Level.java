package game;

/**Classe che rappresenta un livello del gioco. Ogni livello ha il suo World
 *  e i punti ottenuti dal player in quel livello.
 *
 *
 */

public class Level {
    public Level(World world) {
        this.world = world;
    }

    public int levelPoint; // massimo punteggio ottenuto dal player

    public World world;

    /**
     *  ripristino alle condizioni iniziali del livello
     */
    public void reset(){
        world.reset();
    }

}
