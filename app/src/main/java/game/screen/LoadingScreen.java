package game.screen;

import android.util.SparseArray;

import com.badlogic.androidgames.framework.Game;
import com.badlogic.androidgames.framework.Graphics;
import com.badlogic.androidgames.framework.Screen;

import java.util.Random;

import game.Asset;
import game.Background;
import game.BitersGame;
import game.enemy.Boss;
import game.enemy.Enemy;
import game.Level;
import game.Player;
import game.World;
import game.enemy.SimpleAI;
import game.enemy.Zombie;
import game.buttons.Button;
import game.buttons.FireButton;
import game.buttons.JumpButton;
import game.buttons.LeftButton;
import game.buttons.MainMenuButton;
import game.buttons.PauseButton;
import game.buttons.RightButton;
import game.buttons.TryAgainButton;
import game.component.AIComponent;
import game.component.GraphicComponent;
import game.component.MusicComponent;
import game.gameobject.acquirable.Acquirable;
import game.gameobject.acquirable.RedAmmunation;
import util.Constant;

/**
 * La classe LoadingScreen si occupa di effettuare il caricamento
 * di tutti gli elementi di gioco come: asset, livelli ecc..
 * il tutto mentre mostra una schermata di loading all'utente
 *
 *
 */
public class LoadingScreen extends Screen {
    private boolean presented = false;

    public LoadingScreen(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime) {
        //comincio il caricamento dopo aver disegnato la schermata di loading
        if (presented) {
            Graphics g = game.getGraphics();

            //carico gli asset
            loadAssets(g);


            BitersGame bitersGame = (BitersGame) game;

            //carico i livelli
            bitersGame.levels = this.loadLevels();

            //istanzio gli screen
            bitersGame.screens = this.loadScreens();

            //una volta finito setto lo screen del menu principale
            game.setScreen(bitersGame.screens.get(Constant.MENU_SCREEN));
        }
    }

    /**
     * Metodo che genera un nuovo moondo per la demo
     *
     * @return il modno di prova creato
     */
    private World createWorld() {
        Random rand = new Random();
        int randomNum;

        //creo gli oggetti che servono per il mondo di prova
        GraphicComponent backImage = new GraphicComponent(Asset.background,480,320,960,320);
        MusicComponent music = new MusicComponent(Asset.soundtrack);
        Background background = new Background(backImage,music,0,0,480,320);
        Enemy[] zombies = new Enemy[11];
        Boss[] bosses = new Boss[1];
        Acquirable[] acquirables = new Acquirable[1];

        //genero degli zombie in posizioni casuali
        for(int i = 0; i < 10; i++) {
            Zombie zombie = new Zombie(new GraphicComponent(Asset.zombie, 32, 64), new SimpleAI());
            randomNum = rand.nextInt(960);
            while (randomNum < 150)
                randomNum = rand.nextInt(960);
            zombie.absoluteX = randomNum;
            zombie.absoluteY = 178;
            zombie.relativeX = randomNum;
            zombie.relativeY = 178;
            zombie.startX = randomNum;
            zombie.startY = 178;
            zombie.damage = 25;
            zombie.lifePoints = 100;
            zombie.startLifePoints = 100;

            zombies[i] = zombie;
        }

        //genero un boss posizionandolo verso la fine del livello
        Boss boss = new Boss(new GraphicComponent(Asset.zombie, 32, 64), new SimpleAI());
        boss.absoluteX = 920;
        boss.absoluteY = 178;
        boss.relativeX = 920;
        boss.relativeY = 178;
        boss.startX = 920;
        boss.startY = 178;
        boss.lifePoints = 300;
        boss.startLifePoints = 300;
        boss.damage = 50;
        bosses[0] = boss;
        zombies[10] = boss;

        //inserisco un powerup
        acquirables[0] = new RedAmmunation();
        acquirables[0].y = 222;
        acquirables[0].x = 65;
        acquirables[0].startY = 222;
        acquirables[0].startX = 65;

        //una volta generati ghli oggetti che mi servono istanzio il mondo e lo restituisco
        World world = new World(new Player(new GraphicComponent(Asset.player,32,32)), background, zombies, bosses);
        world.acquirables = acquirables;
        return world;
    }


    /**
     * metodo che carica tutti gli asset
     *
     * @param g oggetto Graphics per la creazione di nuovi oggetti Pixmap
     */
    private void loadAssets(Graphics g) {

        //caricamento di tutti gli asset grafici
        Asset.simpleProjectile = g.newPixmap("simpleProjectile.png", Graphics.PixmapFormat.RGB565);
        Asset.redProjectile = g.newPixmap("redProjectile.png", Graphics.PixmapFormat.RGB565);
        Asset.acquireRedProjectile = g.newPixmap("acquireRedProjectile.png", Graphics.PixmapFormat.RGB565);
        Asset.level1 = g.newPixmap("level1_100x100.png", Graphics.PixmapFormat.RGB565);
        Asset.level2 = g.newPixmap("level2_100x100.png", Graphics.PixmapFormat.RGB565);
        Asset.level3 = g.newPixmap("level3_100x100.png", Graphics.PixmapFormat.RGB565);
        Asset.player = g.newPixmap("sheet32x32.png", Graphics.PixmapFormat.RGB565);
        Asset.background = g.newPixmap("background3.png", Graphics.PixmapFormat.RGB565);
        Asset.buttonLeft = g.newPixmap("arrowLeft.png", Graphics.PixmapFormat.RGB565);
        Asset.buttonRight = g.newPixmap("arrowRight.png", Graphics.PixmapFormat.RGB565);
        Asset.buttonFire = g.newPixmap("shootBtn.png", Graphics.PixmapFormat.RGB565);
        Asset.buttonJump = g.newPixmap("arrowUp.png", Graphics.PixmapFormat.RGB565);
        Asset.zombie = g.newPixmap("zombie_n_skeleton32x64.png", Graphics.PixmapFormat.RGB565);
        Asset.startBackground = g.newPixmap("startBackground.png", Graphics.PixmapFormat.RGB565);
        Asset.logo = g.newPixmap("logo.png", Graphics.PixmapFormat.RGB565);
        Asset.zombieMenu = g.newPixmap("zombie1.png", Graphics.PixmapFormat.RGB565);
        Asset.newGame = g.newPixmap("newGame.png", Graphics.PixmapFormat.RGB565);
        Asset.loadGame = g.newPixmap("loadGame.png", Graphics.PixmapFormat.RGB565);
        Asset.options = g.newPixmap("options.png", Graphics.PixmapFormat.RGB565);
        Asset.credits = g.newPixmap("credits.png", Graphics.PixmapFormat.RGB565);
        Asset.scoreIcon = g.newPixmap("zombieHead.png", Graphics.PixmapFormat.RGB565);
        Asset.numbers = g.newPixmap("numbers.png", Graphics.PixmapFormat.RGB565);
        Asset.sound = g.newPixmap("sound.png", Graphics.PixmapFormat.RGB565);
        Asset.on_sound = g.newPixmap("on.png", Graphics.PixmapFormat.RGB565);
        Asset.off_sound = g.newPixmap("off.png", Graphics.PixmapFormat.RGB565);
        Asset.on_effects = g.newPixmap("on.png", Graphics.PixmapFormat.RGB565);
        Asset.off_effects = g.newPixmap("off.png", Graphics.PixmapFormat.RGB565);
        Asset.return_button = g.newPixmap("return.png", Graphics.PixmapFormat.RGB565);
        Asset.options_menu = g.newPixmap("options_menu.png", Graphics.PixmapFormat.RGB565);
        Asset.effects = g.newPixmap("effects.png", Graphics.PixmapFormat.RGB565);
        Asset.zombieOptions = g.newPixmap("zombie2.png", Graphics.PixmapFormat.RGB565);
        Asset.transparentBackground = g.newPixmap("transparentBackground.png", Graphics.PixmapFormat.RGB565);
        Asset.inGamePauseButton = g.newPixmap("pauseButton.png", Graphics.PixmapFormat.RGB565);
        Asset.gameover= g.newPixmap("gameover.png", Graphics.PixmapFormat.RGB565);
        Asset.youWinText = g.newPixmap("youWin.png", Graphics.PixmapFormat.RGB565);
        Asset.mainMenuBtn = g.newPixmap("mainMenuBtn.png", Graphics.PixmapFormat.RGB565);
        Asset.tryAgainBtn = g.newPixmap("tryAgainBtn.png", Graphics.PixmapFormat.RGB565);
        Asset.credits_menu = g.newPixmap("credits_menu.png", Graphics.PixmapFormat.RGB565);
        Asset.zombie_tv = g.newPixmap("zombie_TV.png", Graphics.PixmapFormat.RGB565);

        //caricamento asset sonori
        Asset.soundtrack = game.getAudio().newMusic("music.mp3");
        Asset.soundtrack.setLooping(true);

        Asset.menu_soundtrack = game.getAudio().newMusic("soundtrack1.wav");
        Asset.menu_soundtrack.setLooping(true);

        Asset.fire = game.getAudio().newSound("newlaser.wav");
        Asset.click = game.getAudio().newSound("click.wav");

    }


    /**
     * Metodo che istanzia tutti gli Screen del gioco
     *
     * @return uno SparseArray contenente tutti gli Screen
     */
    private SparseArray<Screen> loadScreens() {
        SparseArray<Screen> screens = new SparseArray();
        screens.put(Constant.LEVEL_SCREEN, new LevelScreen(this.game));
        screens.put(Constant.MENU_SCREEN, new MenuScreen(this.game));
        screens.put(Constant.OPTION_SCREEN, new OptionScreen(this.game));
        screens.put(Constant.CREDIT_SCREEN, new CreditScreen(this.game));

        MainScreen main = new MainScreen(this.game);
        main.runningButtons = createRunningButtons();
        main.menuButtons = createMenuButtons();
        screens.put(Constant.MAIN_SCREEN, main);
        return screens;
    }


    /**
     * Metodo che crea i bottoni della pulsantiera di gioco
     * Sinistra, Destra, Salta, Spara e Pausa.
     *
     * @return array di Button
     */
    private Button[] createRunningButtons() {
        Button[] result = new Button[5];
        GraphicComponent btnLeft = new GraphicComponent(Asset.buttonLeft,50,50);
        GraphicComponent btnRight = new GraphicComponent(Asset.buttonRight,50,50);
        GraphicComponent btnFire = new GraphicComponent(Asset.buttonFire,50,50);
        GraphicComponent btnJump = new GraphicComponent(Asset.buttonJump,50,50);
        GraphicComponent btnPause = new GraphicComponent(Asset.inGamePauseButton, 32, 32);

        result[Constant.FIRE_BUTTON] = new FireButton(Constant.FIRE_BUTTON_X, Constant.FIRE_BUTTON_Y, Constant.FIRE_BUTTON_WIDTH, Constant.FIRE_BUTTON_HEIGHT, btnFire);
        result[Constant.JUMP_BUTTON] = new JumpButton(Constant.JUMP_BUTTON_X, Constant.JUMP_BUTTON_Y, Constant.JUMP_BUTTON_WIDTH, Constant.JUMP_BUTTON_HEIGHT, btnJump);

        result[Constant.LEFT_BUTTON] = new LeftButton(Constant.LEFT_BUTTON_X, Constant.LEFT_BUTTON_Y, Constant.LEFT_BUTTON_WIDTH, Constant.LEFT_BUTTON_HEIGHT, btnLeft);
        result[Constant.RIGHT_BUTTON] = new RightButton(Constant.RIGHT_BUTTON_X, Constant.RIGHT_BUTTON_Y, Constant.RIGHT_BUTTON_WIDTH, Constant.RIGHT_BUTTON_HEIGHT, btnRight);

        result[Constant.PAUSE_BUTTON] = new PauseButton(Constant.PAUSE_BUTTON_X, Constant.PAUSE_BUTTON_Y, Constant.PAUSE_BUTTON_WIDTH, Constant.PAUSE_BUTTON_HEIGHT, btnPause);




        return result;
    }


    /**
     * Metodo che crea i bottoni del menu di pausa relativo alla fase di gioco
     *
     * @return array dei bottoni creati
     */
    private Button[] createMenuButtons() {
        Button[] result = new Button[2];
        GraphicComponent btnMainMenu = new GraphicComponent(Asset.mainMenuBtn,90,30);
        GraphicComponent btnTryAgain = new GraphicComponent(Asset.tryAgainBtn,90,30);


        result[Constant.MAINMENU_BUTTON] = new MainMenuButton(Constant.MAINMENU_BUTTON_X, Constant.MAINMENU_BUTTON_Y, Constant.MAINMENU_BUTTON_WIDTH, Constant.MAINMENU_BUTTON_HEIGHT, btnMainMenu);
        result[Constant.TRYAGAIN_BUTTON] = new TryAgainButton(Constant.TRYAGAIN_BUTTON_X, Constant.TRYAGAIN_BUTTON_Y, Constant.TRYAGAIN_BUTTON_WIDTH, Constant.TRYAGAIN_BUTTON_HEIGHT, btnTryAgain);


        return result;
    }


    /**
     * Metodo che genera i livelli della demo staticamente
     *
     * @return array dei livelli creati
     */
    private Level[] loadLevels() {
        /*
        caricamento dei livelli. Per adesso è statico ma potrebbe
        essere dinamico implementando un loading da file e/o server
         */
        World world = createWorld();
        Level[] levels = new Level[1];

        levels[0] = new Level(world);
        levels[0].world.player.relativeY = 210;
        levels[0].world.player.absoluteY = 210;
        return levels;
    }

    @Override
    public void present(float deltaTime) {
        Graphics g = game.getGraphics();
        Asset.loading = g.newPixmap("loading.png", Graphics.PixmapFormat.RGB565);

        if (!presented) {
            g.drawPixmap(Asset.loading, 0, 0);
            presented = true;
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}