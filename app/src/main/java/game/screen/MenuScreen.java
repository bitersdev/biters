package game.screen;

import com.badlogic.androidgames.framework.Game;
import com.badlogic.androidgames.framework.Graphics;
import com.badlogic.androidgames.framework.Input.TouchEvent;
import com.badlogic.androidgames.framework.Screen;

import java.util.List;

import game.Asset;
import game.BitersGame;
import game.Settings;
import util.Constant;

/**
 * Screen del menu principale. Contiene la prima schermata visualizzata dal player
 * con i tasti per giocare una nuova partita, per andare nel menu delle opzioni e per
 * visualizzare i crediti.
 *
 *
 */

public class MenuScreen extends Screen {
    public MenuScreen(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();


        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        int len = touchEvents.size();
        for (int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if (event.type == TouchEvent.TOUCH_UP) {

                // Sound ON/OFF
                if (inBounds(event, 430, 255, 32, 32)) {
                    Settings.soundEnabled = !Settings.soundEnabled;
                    Asset.menu_soundtrack.stop();
                    if (Settings.soundEnabled && !Asset.menu_soundtrack.isPlaying()) {
                        Asset.menu_soundtrack.play();
                        return;
                    }
                    else if(!Settings.soundEnabled){
                        Asset.menu_soundtrack.stop();
                    }
                }

                // New Game
                if (inBounds(event, 350, 100, 90, 30)) {
                    BitersGame bitersGame = (BitersGame) game;
                    bitersGame.setScreen(bitersGame.screens.get(Constant.LEVEL_SCREEN));
                    if (Settings.effectsEnabled) {
                        Asset.click.play(1);
                        return;
                    }
                }

                // Options
                if (inBounds(event, 350, 150, 90, 30)) {
                    BitersGame bitersGame = (BitersGame) game;
                    bitersGame.setScreen(bitersGame.screens.get(Constant.OPTION_SCREEN));
                    if (Settings.soundEnabled) {
                        Asset.menu_soundtrack.play();
                    }
                    if (Settings.effectsEnabled) {
                        Asset.click.play(1);
                        return;
                    }
                }

                // Credits
                if (inBounds(event, 350, 200, 90, 30)) {
                    BitersGame bitersGame = (BitersGame) game;
                    bitersGame.setScreen(bitersGame.screens.get(Constant.CREDIT_SCREEN));
                    if (Settings.soundEnabled) {
                        Asset.menu_soundtrack.play();
                    }
                    if (Settings.effectsEnabled) {
                        Asset.click.play(1);
                        return;
                    }
                }

            }
        }
    }

    @Override
    public void present(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawPixmap(Asset.startBackground, 0, 0);
        g.drawPixmap(Asset.logo, -5, 0);
        g.drawPixmap(Asset.zombieMenu, -10, 50);
        g.drawPixmap(Asset.newGame, 350, 100);
        g.drawPixmap(Asset.options, 350, 150);
        g.drawPixmap(Asset.credits, 350, 200);
        g.drawPixmap(Asset.sound, 335, 250);
        if(Settings.soundEnabled)
            g.drawPixmap(Asset.on_sound, 430, 255);
        else
            g.drawPixmap(Asset.off_sound, 430, 255);
    }

    private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
        return event.x > x && event.x < x + width - 1 &&
                event.y > y && event.y < y + height - 1;
    }

    @Override
    public void pause() {
        Asset.menu_soundtrack.stop();
    }

    @Override
    public void resume() {
        Graphics g = game.getGraphics();
        if(Settings.soundEnabled) {
            g.drawPixmap(Asset.on_sound, 430, 255);
            Asset.menu_soundtrack.play();
            Asset.menu_soundtrack.setLooping(true);
        }
        else
            g.drawPixmap(Asset.off_sound, 430, 255);
    }

    @Override
    public void dispose() {
        Asset.menu_soundtrack.stop();
    }
}
