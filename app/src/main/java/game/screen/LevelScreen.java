package game.screen;

import android.content.Context;
import android.content.SharedPreferences;

import com.badlogic.androidgames.framework.Game;
import com.badlogic.androidgames.framework.Graphics;
import com.badlogic.androidgames.framework.Input;
import com.badlogic.androidgames.framework.Screen;

import java.util.List;

import game.Asset;
import game.BitersGame;
import game.Settings;
import util.Constant;

/**
 * Screen di selezione dei livelli dove vengono disegnati i riquadri di selezione
 * dei livelli
 *
 *
 */

public class LevelScreen extends Screen {
    public LevelScreen(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime) {
        List<Input.TouchEvent> events = game.getInput().getTouchEvents();
        // go to selected level
        for (int i = 0; i < events.size(); i++) {
            Input.TouchEvent ev = events.get(i);
            //tap sul livello 1
            if (ev.type == Input.TouchEvent.TOUCH_DOWN) {
                if (ev.x > 70 && ev.x < 170 && ev.y > 110 && ev.y < 210) {
                    if (Settings.effectsEnabled) {
                        Asset.click.play(1);
                    }
                    BitersGame bitersGame = (BitersGame) game;
                    bitersGame.status = BitersGame.RUNNING;
                    MainScreen mainScreen = (MainScreen) bitersGame.screens.get(Constant.MAIN_SCREEN);
                    mainScreen.world = bitersGame.levels[0].world;
                    mainScreen.world.reset();
                    game.setScreen(mainScreen);

                }
                //tap sul livello 2
                if (ev.x > 190 && ev.x < 290 && ev.y > 110 && ev.y < 210) {

                }
                //tap sul livello 3
                if (ev.x > 310 && ev.x < 410 && ev.y > 110 && ev.y < 210) {

                }
            }
        }
    }

    @Override
    public void present(float deltaTime) {
        BitersGame bitersGame = (BitersGame) this.game;

        //recupero l'highscore
        SharedPreferences sharedPref =  bitersGame.getPreferences(Context.MODE_PRIVATE);
        int highscore = sharedPref.getInt(Constant.LEVEL1_HIGHSCORE, 0);
        Graphics g = game.getGraphics();
        g.drawPixmap(Asset.startBackground, 0, 0);

        // disegno i riquadri
        g.drawPixmap(Asset.level1, 70, 110);
        g.drawPixmap(Asset.scoreIcon, 70,220);
        drawText(g, Integer.toString(highscore), 70 + 32, 220);

        g.drawPixmap(Asset.level2, 190, 110);
        g.drawPixmap(Asset.level3, 310, 110);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        Asset.menu_soundtrack.stop();
    }


    /**
     * metodo per disegnare testo e numeri
     *
     * @param g oggetto Graphics
     * @param line stringa da disegnare
     * @param x x dove cominciare a disegnare
     * @param y y dove cominciare a disegnare
     */
    public void drawText(Graphics g, String line, int x, int y) {
        int len = line.length();
        for (int i = 0; i < len; i++) {
            char character = line.charAt(i);

            if (character == ' ') {
                x += 20;
                continue;
            }

            int srcX = 0;
            int srcWidth = 0;
            if (character == '.') {
                srcX = 200;
                srcWidth = 10;
            } else {
                srcX = (character - '0') * 20;
                srcWidth = 20;
            }

            g.drawPixmap(Asset.numbers, x, y, srcX, 0, srcWidth, 32);
            x += srcWidth;
        }
    }
}
