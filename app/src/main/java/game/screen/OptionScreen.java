package game.screen;

import com.badlogic.androidgames.framework.Game;
import com.badlogic.androidgames.framework.Graphics;
import com.badlogic.androidgames.framework.Input;
import com.badlogic.androidgames.framework.Screen;

import java.util.List;

import game.Asset;
import game.BitersGame;
import game.Settings;
import util.Constant;

/**
 * Screen del menu delle opzioni dove è possibile attivare/disattivare suoni ed effetti sonori
 *
 *
 */

public class OptionScreen extends Screen {
    public OptionScreen(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();


        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        int len = touchEvents.size();
        for (int i = 0; i < len; i++) {
            Input.TouchEvent event = touchEvents.get(i);
            if (event.type == Input.TouchEvent.TOUCH_UP) {

                // Return Menu Screen
                if (inBounds(event, 20, 255, 32, 32)) {
                    BitersGame bitersGame = (BitersGame) game;
                    bitersGame.setScreen(bitersGame.screens.get(Constant.MENU_SCREEN));
                }

                // Sound ON/OFF
                if (inBounds(event, 150, 105, 32, 32)) {
                    Settings.soundEnabled = !Settings.soundEnabled;
                    Asset.menu_soundtrack.stop();
                    if (Settings.soundEnabled) {
                        Asset.menu_soundtrack.play();
                        return;
                    }
                }

                // Effects ON/OFF
                if (inBounds(event, 150, 155, 80, 40)) {
                    Settings.effectsEnabled = !Settings.effectsEnabled;
                    if (Settings.effectsEnabled) {
                        Asset.click.play(1);
                        return;
                    }
                }
            }
        }
    }

    private boolean inBounds(Input.TouchEvent event, int x, int y, int width, int height) {
        return event.x > x && event.x < x + width - 1 &&
                event.y > y && event.y < y + height - 1;
    }

    @Override
    public void present(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawPixmap(Asset.startBackground, 0, 0);
        g.drawPixmap(Asset.options_menu, 10, 0);
        g.drawPixmap(Asset.zombieOptions, 270, 30);

        g.drawPixmap(Asset.sound, 30, 100);
        g.drawPixmap(Asset.effects, 30, 150);

        g.drawPixmap(Asset.return_button, 20, 255);


        if(Settings.soundEnabled)
            g.drawPixmap(Asset.on_sound, 150, 105);
        else
            g.drawPixmap(Asset.off_sound, 150, 105);

        if(Settings.effectsEnabled)
            g.drawPixmap(Asset.on_effects, 150, 155);
        else
            g.drawPixmap(Asset.off_effects, 150, 155);
    }

    @Override
    public void pause() {
        Asset.menu_soundtrack.stop();
    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        Asset.menu_soundtrack.stop();
    }
}
