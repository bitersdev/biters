package game.screen;

/**
 * Screen di gioco: è quello che si occupa di disegnare il mondo relativo al
 * livello selezionato e di definire i metodi di aggiornamento e visualizzazione del mondo
 *
 *
 */

import android.content.Context;
import android.content.SharedPreferences;

import com.badlogic.androidgames.framework.Game;
import com.badlogic.androidgames.framework.Graphics;
import com.badlogic.androidgames.framework.Input;
import com.badlogic.androidgames.framework.Screen;
import java.util.List;

import game.Asset;
import game.BitersGame;
import game.enemy.Boss;
import game.enemy.Enemy;
import game.Player;
import game.Settings;
import game.World;
import game.enemy.Zombie;
import game.component.GraphicComponent;
import game.component.MusicComponent;
import game.gameobject.acquirable.Acquirable;
import game.interfaces.Jumper;
import game.interfaces.Walker;
import game.buttons.Button;
import game.weapons.Projectile;
import util.Constant;

public class MainScreen extends Screen{

    private final Graphics graphics;
    public World world; //modello del mondo
    public int lastWalkPointer = -1; // ultimo puntatore di un tocco relativo al movimento
    public int lastFirePointer = -1; // ultimo puntatore di un tocco relativo allo sparo
    public Button[] runningButtons; // pulsantiera di gioco
    public Button[] menuButtons; // bottoni del menu di pausa


    /**
     * costruttore del MainScreen
     * @param game
     */
    public MainScreen(Game game) {
        super(game);
        graphics = game.getGraphics();

    }

    public BitersGame getGame(){
        return (BitersGame) this.game;
    }

    @Override
    public void update(float deltaTime) {
        List<Input.TouchEvent> events = game.getInput().getTouchEvents();
        BitersGame bitersGame = (BitersGame) game;

        //se il player è morto metto il gioco nello stato di game over
        if(this.world.player.isDead()) {
            bitersGame.status = BitersGame.GAMEOVER;
        }else{ //se il player non è morto ma sono morti tutti i boss allora metto il gioco nello stato di vittoria
            boolean bossesAreDead = true;
            for (Boss b : world.bosses) {
                if(b.isAlive()) {
                    bossesAreDead = false;
                    break;
                }
            }
            if(bossesAreDead){
                bitersGame.status = BitersGame.WIN;
            }
        }

        // se il gioco è nello stato di RUNNING richiamo i metodi di update del player e del mondo
        if(bitersGame.status == BitersGame.RUNNING) {
            //update dello stato del player
            this.updatePlayer(events, deltaTime, world.backgroundScrolling);
            // update del mondo
            world.update(events, deltaTime);
        }
        else if(bitersGame.status == BitersGame.PAUSE){
            //se il gioco è in pausa richiamo il metodo relativo al menu di pausa
            this.updatePauseMenu(events);
        }
        else if(bitersGame.status == BitersGame.GAMEOVER){
            // se lo stato è GAMEOVER richiamo il metodo relativo
            this.updateGameOverMenu(events);
        }
        else if(bitersGame.status == BitersGame.WIN){
            //recupero l'highscore
            SharedPreferences sharedPref =  bitersGame.getPreferences(Context.MODE_PRIVATE);
            int highscore = sharedPref.getInt(Constant.LEVEL1_HIGHSCORE, 0);

            //se ho superato l'highscore lo aggiorno col punteggio corrente
            if (highscore < world.player.score) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt(Constant.LEVEL1_HIGHSCORE, this.world.player.score);
                editor.commit();
            }

            //se lo stato è WIN richiamo il metodo relativo
            this.updateWinMenu(events);
        }


    }


    /**
     * metodo che controlla se vengono premuti tasti all'interno del
     * menu relativo allo stato di GAMEOVER
     *
     * @param events eventi touch
     */
    private void updateGameOverMenu(List<Input.TouchEvent> events) {
        for (int i = 0; i < events.size(); i++) {
            Input.TouchEvent ev = events.get(i);
            if (ev.type == Input.TouchEvent.TOUCH_DOWN) {
                for (int j = 0; j < menuButtons.length; j++) {
                    Button current = menuButtons[j];
                    if (current.inBounds(ev.x, ev.y)) {
                        current.press(world.player, this, 0, false, ev.pointer);
                    }
                }
            }
        }
    }



    /**
     * metodo che controlla se vengono premuti tasti all'interno del
     * menu relativo allo stato di WIN
     *
     * @param events eventi touch
     */
    private void updateWinMenu(List<Input.TouchEvent> events) {
        for (int i = 0; i < events.size(); i++) {
            Input.TouchEvent ev = events.get(i);
            if (ev.type == Input.TouchEvent.TOUCH_DOWN) {
                for (int j = 0; j < menuButtons.length; j++) {
                    Button current = menuButtons[j];
                    if (current.inBounds(ev.x, ev.y)) {
                        current.press(world.player, this, 0, false, ev.pointer);
                    }
                }
            }
        }
    }



    private boolean inBounds(Input.TouchEvent event, int x, int y, int width, int height) {
        return event.x > x && event.x < x + width - 1 &&
                event.y > y && event.y < y + height - 1;
    }


    /**
     * metodo che controlla se vengono premuti tasti all'interno del menu
     * relativo allo stato di PAUSE
     *
     * @param events eventi touch
     */
    private void updatePauseMenu(List<Input.TouchEvent> events) {
        int len = events.size();
        for (int i = 0; i < len; i++) {
            Input.TouchEvent event = events.get(i);
            if (event.type == Input.TouchEvent.TOUCH_DOWN) {

                // Return Menu Screen
                if (inBounds(event, 440, 10, 32, 32)) {
                    BitersGame bitersGame = (BitersGame) game;
                    bitersGame.status = BitersGame.RUNNING;
                }

                // Sound ON/OFF
                if (inBounds(event, 279, 80, 32, 32)) {
                    Settings.soundEnabled = !Settings.soundEnabled;
                    if (Settings.soundEnabled) {
                        Asset.soundtrack.play();
                        return;
                    }
                    else{
                        Asset.soundtrack.stop();

                    }
                }

                // Effects ON/OFF
                if (inBounds(event, 279, 130, 80, 40)) {
                    Settings.effectsEnabled = !Settings.effectsEnabled;
                    if (Settings.effectsEnabled) {
                        Asset.click.play(1);
                        return;
                    }
                }

                for (int j = 0; j < menuButtons.length; j++) {
                    Button current = menuButtons[j];
                    if (current.inBounds(event.x, event.y)) {
                        current.press(world.player, this, 0, false, event.pointer);
                    }
                }


            }
        }
    }


    /**
     * metodo che verifica la pressione dei tasti e aggiorna lo stato del player di conseguenza
     *
     * @param events eventi touch
     * @param deltaTime tempo trascorso dall'ultimo frame
     * @param backgroundScrolling stato di scorrimento dello sfondo
     */
    private void updatePlayer(List<Input.TouchEvent> events, float deltaTime, boolean backgroundScrolling) {
        GraphicComponent backImage = (GraphicComponent) world.background.getComponent(Constant.GRAPHIC_COMPONENT);

        //scandisco gli eventi touch e ne propago gli effetti sul mondo
        for (int i = 0; i < events.size(); i++) {
            Input.TouchEvent ev = events.get(i);
            if(ev.type == Input.TouchEvent.TOUCH_DOWN) {
                for(int j = 0; j < runningButtons.length; j++){
                    Button current = runningButtons[j];
                    if (current.inBounds(ev.x,ev.y)){
                        //se un evento touch corrisponde ad un bottone ne scateno il metodo press
                        current.press(world.player, this, deltaTime, backgroundScrolling, ev.pointer);
                    }
                }
            }
            else if(ev.pointer == lastWalkPointer && ev.type == Input.TouchEvent.TOUCH_UP){
                lastWalkPointer = -1; // se rilascio il tasto di spostamento setto il pointer a -1
            }
            else if(ev.pointer == lastFirePointer && ev.type == Input.TouchEvent.TOUCH_UP){
                lastFirePointer = -1; // se rilascio il tasto di attacco setto il pointer a -1
            }
        }

        //controllo se il tasto di spostamento non è stato rilasciato
        if(lastWalkPointer != -1){
            if (world.player.lastDirection == Walker.DIR_RIGHT){
                if(world.player.absoluteX < backImage.width - 32)
                    world.player.updateWalk(Walker.DIR_RIGHT, deltaTime, backgroundScrolling);
            }
            else if (world.player.lastDirection == Walker.DIR_LEFT){
                if(world.player.absoluteX > world.background.currentX)
                    world.player.updateWalk(Walker.DIR_LEFT, deltaTime, backgroundScrolling);
            }
        }
        else{
            world.player.updateStand(deltaTime);
        }


        // controllo se il tasto di fuoco non è stato rilasciato
        if(lastFirePointer != -1){
            runningButtons[Constant.FIRE_BUTTON].press(world.player, this, deltaTime, backgroundScrolling, lastFirePointer);
        }

        //controllo se sono ancora in fase di salto
        if(world.player.jumpStatus == Jumper.JUMP){
            world.player.updateJump(deltaTime);
        }
    }


    @Override
    public void present(float deltaTime) {

        //draw background
        drawBackground();

        BitersGame bitersGame = (BitersGame) game;

        //Se il gioco è in esecuzione  disegno tutti gli elementi
        if(bitersGame.status == BitersGame.RUNNING) {
            for (int i = 0; i < world.activeProjectiles.size(); i++) {
                drawProjectile(world.activeProjectiles.valueAt(i));
            }
            //draw player
            drawPlayer(deltaTime);


            //draw Zombie
            drawEnemies(deltaTime, world.enemies);

            //draw Acquirables
            drawAcquirables();

            //draw button
            for (int i = 0; i < runningButtons.length; i++) {
                graphics.drawPixmap(runningButtons[i].image.image, runningButtons[i].positionX, runningButtons[i].positionY);
            }
        }

        //se il gioco è in pausa disegno il menu della pausa
        else if(bitersGame.status == BitersGame.PAUSE){
            this.drawPauseMenu();
        }

        //se il gioco è in game over disegno il menu di gam over
        else if(bitersGame.status == BitersGame.GAMEOVER){
            this.drawGameOverMenu();
        }

        //se il gioco è nello stati di vittoria disegno il menu della vittoria
        else if(bitersGame.status == BitersGame.WIN){
            this.drawWinMenu();
        }

        //draw score
        graphics.drawPixmap(Asset.scoreIcon, Constant.SCORE_ICON_X, Constant.SCORE_ICON_Y);
        drawText(graphics, Integer.toString(world.player.score), Constant.SCORE_ICON_X + 32, Constant.SCORE_ICON_Y);

    }


    /**
     * Metodo che si occupa di disegnare il menu di pausa
     *
     */
    private void drawPauseMenu() {
        graphics.drawPixmap(Asset.transparentBackground, 0, 0);
        graphics.drawPixmap(Asset.options, 195, 10);

        graphics.drawPixmap(Asset.sound, 169, 75);
        graphics.drawPixmap(Asset.effects, 169, 125);

        graphics.drawPixmap(Asset.return_button, 440, 10);


        if(Settings.soundEnabled)
            graphics.drawPixmap(Asset.on_sound, 279, 80);
        else
            graphics.drawPixmap(Asset.off_sound, 279, 80);

        if(Settings.effectsEnabled)
            graphics.drawPixmap(Asset.on_effects, 279, 130);
        else
            graphics.drawPixmap(Asset.off_effects, 279, 130);


        //draw button
        for (int i = 0; i < menuButtons.length; i++) {
            graphics.drawPixmap(menuButtons[i].image.image, menuButtons[i].positionX, menuButtons[i].positionY);
        }
    }


    /**
     * Metodo che si occupa di disegnare il menu di GAMEOVER
     */
    private void drawGameOverMenu() {
        graphics.drawPixmap(Asset.transparentBackground, 0, 0);
        graphics.drawPixmap(Asset.gameover, 60, 80);

        //draw button
        for (int i = 0; i < menuButtons.length; i++) {
            graphics.drawPixmap(menuButtons[i].image.image, menuButtons[i].positionX, menuButtons[i].positionY);
        }


    }


    /**
     * Metodo che si occupa di disegnare il  menu di WIN
     */
    private void drawWinMenu() {
        graphics.drawPixmap(Asset.transparentBackground, 0, 0);
        graphics.drawPixmap(Asset.youWinText, 60, 80);
        //draw button
        for (int i = 0; i < menuButtons.length; i++) {
            graphics.drawPixmap(menuButtons[i].image.image, menuButtons[i].positionX, menuButtons[i].positionY);
        }

    }


    /**
     *  metodo che si occupa di disegnare un proiettile
     * @param p proiettile da disegnare
     */
    private void drawProjectile(Projectile p) {
        if(!p.active) return;
        graphics.drawPixmap(p.pixmap, p.x,p.y);
    }



    /**
     * metodo che si occupa di disegnare gli oggetti acquisibili
     */
    private void drawAcquirables() {
        for (Acquirable a : world.acquirables){
            if(!a.acquired)
                graphics.drawPixmap(a.graphic.image, a.x,a.y);
        }
    }



    @Override
    public void pause() {
        //se l'activity va in background blocco la musica e metto il gioco in pausa
        MusicComponent musicComp = (MusicComponent) this.world.background.getComponent(Constant.MUSIC_COMPONENT);
        musicComp.music.stop(); //fermo la musica

        //assegno lo stato di pausa
        BitersGame g = (BitersGame) game;
        if(g.status != BitersGame.GAMEOVER)
            g.status = BitersGame.PAUSE;
    }

    @Override
    public void resume() {
        // fermo la musica del menu
        Asset.menu_soundtrack.stop();
        if (Settings.soundEnabled) {
            //se la musica è abilitato faccio partire la componente musicale del livello
            MusicComponent musicComp = (MusicComponent) this.world.background.getComponent(Constant.MUSIC_COMPONENT);
            musicComp.music.setLooping(true);
            musicComp.music.play();
        }
    }

    @Override
    public void dispose() {
        //se chiudo l'activity fermo la musica
        MusicComponent musicComp = (MusicComponent) this.world.background.getComponent(Constant.MUSIC_COMPONENT);
        musicComp.music.stop(); //fermo la musica
    }


    /**
     * Metodo che si occupa di disegnare i nemici
     *
     * @param deltaTime tempo trascorso dall'ultimo frame
     * @param zombies array di nemici da disegnare
     */
    private void drawEnemies(float deltaTime, Enemy[] zombies) {
        for(int i = 0; i < zombies.length; i++){
            if(zombies[i].status == Zombie.WALK)
                zombies[i].presentWalk(game,deltaTime);
            else if (zombies[i].status == Zombie.STAND)
                zombies[i].presentStand(game,deltaTime);

        }
    }


    /**
     * Metodo che si occupa di disegnare lo sfondo
     */
    private void drawBackground() {
        GraphicComponent background = (GraphicComponent) world.background.getComponent(Constant.GRAPHIC_COMPONENT);
        graphics.drawPixmap(background.image, 0, 0, world.background.currentX, 0, background.windowWidth +1, background.windowHeight +1);
    }


    /**
     * metodo che si occupa di disegnare il player
     *
     * @param deltaTime tempo trascorso dall'ultimo frame
     */
    private void drawPlayer(float deltaTime){
        graphics.drawRect(10,20,  100, 10, android.graphics.Color.RED);
        graphics.drawRect(10,20, world.player.lifePoints, 10, android.graphics.Color.GREEN);


        //in base allo stato del player richiamo il relativo metodo di present
        if(world.player.status == Player.WALK && world.player.jumpStatus != Jumper.JUMP) {
            world.player.presentWalk(game, deltaTime);
        }
        else if(world.player.jumpStatus == Jumper.JUMP) {
            world.player.presentJump(game,deltaTime);
        }
        else if(world.player.status == Player.STAND){
            world.player.presentStand(game, deltaTime);
        }
    }


    /**
     * metodo per disegnare testo e numeri
     *
     * @param g oggetto Graphics
     * @param line stringa da disegnare
     * @param x x dove cominciare a disegnare
     * @param y y dove cominciare a disegnare
     */
    public void drawText(Graphics g, String line, int x, int y) {
        int len = line.length();
        for (int i = 0; i < len; i++) {
            char character = line.charAt(i);

            if (character == ' ') {
                x += 20;
                continue;
            }

            int srcX = 0;
            int srcWidth = 0;
            if (character == '.') {
                srcX = 200;
                srcWidth = 10;
            } else {
                srcX = (character - '0') * 20;
                srcWidth = 20;
            }

            g.drawPixmap(Asset.numbers, x, y, srcX, 0, srcWidth, 32);
            x += srcWidth;
        }
    }


    /**
     * metodo che verifica il raggiungimento del margine sinistro dell'area gioco
     *
     * @return true se raggiuntio, false altrimenti
     */
    public boolean leftMarginReached(){
        return world.player.absoluteX <= world.background.currentX;
    }


    /**
     * metodo che verifica il raggiungimento del margine destro dell'area di gioco
     *
     * @return true se raggiunto, false altrimenti
     */
    public boolean rightMarginReached(){
        GraphicComponent gc = (GraphicComponent) world.player.getComponent(Constant.GRAPHIC_COMPONENT);
        GraphicComponent backImage = (GraphicComponent) world.background.getComponent(Constant.GRAPHIC_COMPONENT);
        return world.player.absoluteX >= backImage.width - gc.windowWidth;
    }
}
