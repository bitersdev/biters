package game.screen;

import com.badlogic.androidgames.framework.Game;
import com.badlogic.androidgames.framework.Graphics;
import com.badlogic.androidgames.framework.Input;
import com.badlogic.androidgames.framework.Screen;

import java.util.List;

import game.Asset;
import game.BitersGame;
import game.Settings;
import util.Constant;

/**
 * Screen relativo ai credits dove vengono mensionati i
 * nomi dei programmatori
 *
 *
 */

public class CreditScreen extends Screen {
    public CreditScreen(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();


        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        int len = touchEvents.size();
        for (int i = 0; i < len; i++) {
            Input.TouchEvent event = touchEvents.get(i);
            if (event.type == Input.TouchEvent.TOUCH_UP) {

                // Return Menu Screen
                if (inBounds(event, 430, 280, 32, 32)) {
                    BitersGame bitersGame = (BitersGame) game;
                    bitersGame.setScreen(bitersGame.screens.get(Constant.MENU_SCREEN));
                }

            }
        }
    }

    private boolean inBounds(Input.TouchEvent event, int x, int y, int width, int height) {
        return event.x > x && event.x < x + width - 1 &&
                event.y > y && event.y < y + height - 1;
    }

    @Override
    public void present(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawPixmap(Asset.startBackground, 0, 0);
        g.drawPixmap(Asset.credits_menu, 0, 0);
        g.drawPixmap(Asset.zombie_tv, 10, 0);
        g.drawPixmap(Asset.return_button, 430, 280);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

}
