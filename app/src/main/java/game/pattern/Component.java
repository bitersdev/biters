package game.pattern;

/**
 * Component dal design pattern Entity-Component
 *
 *
 */
public abstract class Component {
    protected Entity owner;


    /**
     * get del proprietario
     *
     * @return
     */
    public Entity getOwner() {
        return owner;
    }

    /**
     * set del proprietario
     *
     * @param owner Entity prorpietario della componente
     */
    public void setOwner(Entity owner) {
        this.owner = owner;
    }
}
