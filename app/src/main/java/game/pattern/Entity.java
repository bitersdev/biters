package game.pattern;

import android.util.SparseArray;

/**
 * Entity dal design pattern Entity-Component
 *
 *
 */
public abstract class Entity {
    private SparseArray<Component> component = new SparseArray<Component>();

    /**
     *aggiunge una nuova componente
     *
     * @param index indice della lista
     * @param newComponent componente da aggiungere
     */
    public void addComponent(int index, Component newComponent){
        this.component.put(index,newComponent);
        newComponent.setOwner(this);
    }

    public Component getComponent(int index){
        return this.component.get(index);
    }
}
