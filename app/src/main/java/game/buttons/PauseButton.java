package game.buttons;

import game.BitersGame;
import game.Player;
import game.component.GraphicComponent;
import game.screen.MainScreen;

/**
 * Classe che implementa il tasto pausa
 *
 *
 */

public class PauseButton extends Button{

    public PauseButton(int positionX, int positionY, int width, int height, GraphicComponent image) {
        super(positionX, positionY, width, height, image);
    }

    @Override
    public void press(Player player, MainScreen screen, float deltaTime, boolean backgroundScrolling, int pointer) {
        screen.getGame().status = BitersGame.PAUSE;
    }
}
