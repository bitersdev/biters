package game.buttons;

import game.Player;
import game.component.GraphicComponent;
import game.interfaces.Walker;
import game.screen.MainScreen;
import game.weapons.SimpleProjectile;

/**
 * Classe che implementa il tasto per sparare
 *
 *
 */

public class FireButton extends Button {

    private float timeBeforeNextShoot = 0.2f;
    private boolean canShoot = true;
    private float startTime;


    public FireButton(int positionX, int positionY, int width, int height, GraphicComponent image) {
        super(positionX, positionY, width, height, image);

    }

    @Override
    public void press(Player player, MainScreen screen, float deltaTime, boolean backgroundScrolling, int pointer)
    {
        /* quando sparo, registro quando ho sparato e disabilito il bottone, poi quando lo ripremo
           vedo quanto tempo è passato dall'ultimo sparo: se ne è passato abbastanza, riattivo)
         */
        float timeElapsed = (System.nanoTime()-startTime) / 1000000000.0f;
        if(timeElapsed > timeBeforeNextShoot) {
            canShoot = true;
        }

        if (canShoot) {
            int direction = player.lastDirection;
            int x;
            int y;

            if(direction == Walker.DIR_RIGHT)
                x = player.relativeX + 20;
            else
                x = player.relativeX - 12;

            y = player.relativeY;

            player.getWeapon().fire(x, y, direction, screen.world.activeProjectiles);
            startTime = System.nanoTime();
            canShoot = false;
            screen.lastFirePointer = pointer;
        }

    }
}
