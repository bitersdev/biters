package game.buttons;

import game.component.GraphicComponent;
import game.screen.MainScreen;

import game.Player;

/**
 * Classe per la generalizzazione dei bottoni.
 * fonrnisce metodi standard per individuare la pressione ed eseguire l'azione del bottone
 *
 */

public abstract class Button {
    public int positionX;
    public int positionY;
    public int width;
    public int height;
    public GraphicComponent image;


    /**
     *  costruttore del bottone
     *
     * @param positionX y
     * @param positionY x
     * @param width larghezza
     * @param height altezza
     * @param image componente grafica
     */
    public Button(int positionX, int positionY, int width, int height, GraphicComponent image ) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.width = width;
        this.height = height;
        this.image = image;
    }


    /**
     * Definisce l'azione da eseguire quando il tasto viene premuto
     *
     * @param player player
     * @param screen screen di origine
     * @param deltaTime tempo trascorso
     * @param backgroundScrolling stato dello scorrimento dello sfondo
     * @param pointer puntatore dell'evento touch
     */
    public abstract void press(Player player, MainScreen screen, float deltaTime, boolean backgroundScrolling, int pointer);


    /**
     * restituisce true se le coordinate passate rientrano nei confini de bottone
     *
     * @param x
     * @param y
     * @return
     */
    public boolean inBounds(int x, int y){
        boolean result = false;
        if(x >= this.positionX && x <= (this.positionX + this.width) && y >= this.positionY && y < (this.positionY + this.height)){
            result = true;
        }
        return result;
    }

}
