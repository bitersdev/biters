package game.buttons;

import com.badlogic.androidgames.framework.Game;

import game.BitersGame;
import game.Player;
import game.component.GraphicComponent;
import game.screen.MainScreen;
import util.Constant;

/**
 * Classe che implementa il tasto per tornare al menu principale
 *
 *
 */

public class MainMenuButton extends Button{
    public MainMenuButton(int positionX, int positionY, int width, int height, GraphicComponent image) {
        super(positionX, positionY, width, height, image);
    }

    @Override
    public void press(Player player, MainScreen screen, float deltaTime, boolean backgroundScrolling, int pointer) {
        //reset dei pointer
        screen.lastFirePointer = -1;
        screen.lastWalkPointer = -1;

        //set dello screen
        BitersGame g = (BitersGame) screen.getGame();
        g.setScreen(g.screens.get(Constant.MENU_SCREEN));
    }
}
