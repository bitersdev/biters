package game.buttons;


import game.Player;
import game.component.GraphicComponent;
import game.interfaces.Jumper;
import game.screen.MainScreen;

/**
 * classe che implementa il tasto per il salto
 *
 *
 */

public class JumpButton extends Button {
    public JumpButton(int positionX, int positionY, int width, int height, GraphicComponent image) {
        super(positionX, positionY, width, height, image);
    }

    @Override
    public void press(Player player, MainScreen screen, float deltaTime, boolean backgroundScrolling, int pointer) {
        //posso saltare solo se non sto già saltando
        if(player.jumpStatus != Jumper.JUMP) {
            player.startY = player.absoluteY;
            player.updateJump(deltaTime);
        }
    }
}
