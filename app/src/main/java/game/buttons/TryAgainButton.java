package game.buttons;

import game.BitersGame;
import game.Player;
import game.component.GraphicComponent;
import game.screen.MainScreen;

/**
 * Classe che implementa il tasto per riprovare l'ultimo livello giocato
 *
 *
 */

public class TryAgainButton extends Button {
    public TryAgainButton(int positionX, int positionY, int width, int height, GraphicComponent image) {
        super(positionX, positionY, width, height, image);
    }

    @Override
    public void press(Player player, MainScreen screen, float deltaTime, boolean backgroundScrolling, int pointer) {
        BitersGame g = (BitersGame) screen.getGame();
        //reset di puntatori e del mondo
        screen.lastFirePointer = -1;
        screen.lastWalkPointer = -1;
        screen.world.reset();

        //ritorno allo stato di running
        g.status = BitersGame.RUNNING;

    }
}
