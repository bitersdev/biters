package game.buttons;


import game.Player;
import game.component.GraphicComponent;
import game.interfaces.Walker;
import game.screen.MainScreen;

/**
 * CLasse che implementa il tasto per muoversi vestro sinistra
 *
 *
 */

public class LeftButton extends Button {
    public LeftButton(int positionX, int positionY, int width, int height, GraphicComponent image) {
        super(positionX, positionY, width, height, image);
    }

    @Override
    public void press(Player player, MainScreen screen, float deltaTime, boolean backgroundScrolling, int pointer) {
        //controllo se ho raggiunto il limite sinistro dello schermo
        if(!screen.leftMarginReached())
            player.updateWalk(Walker.DIR_LEFT, deltaTime, backgroundScrolling);
        player.lastDirection = Walker.DIR_LEFT;
        screen.lastWalkPointer = pointer;
    }
}
