package game.buttons;

import game.component.GraphicComponent;
import game.interfaces.Walker;
import game.screen.MainScreen;

import game.Player;

/**
 * Classe che implementa il tasto dello spostamento verso destra
 *
 *
 */

public class RightButton extends Button {
    public RightButton(int positionX, int positionY, int width, int height, GraphicComponent image) {
        super(positionX, positionY, width, height, image);
    }

    @Override
    public void press(Player player, MainScreen screen, float deltaTime, boolean backgroundScrolling, int pointer) {
        if(!screen.rightMarginReached())
            player.updateWalk(Walker.DIR_RIGHT, deltaTime, backgroundScrolling);
        player.lastDirection = Walker.DIR_RIGHT;
        screen.lastWalkPointer = pointer;
    }
}
