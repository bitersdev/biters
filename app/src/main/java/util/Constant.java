package util;

/**
 * Define constants for the game.
 *
 *
 */
public class Constant {

    /*COSTANTI CHE DEFINISCONO GLI INDICI DELLE COMPONENTI
    * ALL'INTERNO DELLO SPARSEARRAY*/
    public static final int GRAPHIC_COMPONENT = 0;
    public static final int AI_COMPONENT = 1;
    public static final int MUSIC_COMPONENT = 2;
    public static final int SOUND_COMPONENT = 3;


    /*COSTANTI CHE DEFINISCONO GLI INDICI DEGLI SCREEN ALL'INTERNO
    * DELLO SPARSEARRAY*/
    public static final int LOADING_SCREEN = 0;
    public static final int LEVEL_SCREEN = 1;
    public static final int MENU_SCREEN = 2;
    public static final int OPTION_SCREEN = 3;
    public static final int MAIN_SCREEN = 4;
    public static final int CREDIT_SCREEN = 5;

    /*COSTANTI DEGLI INDICI DEI BOTTONI DI GIOCO NELL'ARRAY*/
    public static final int RIGHT_BUTTON = 0;
    public static final int LEFT_BUTTON = 1;
    public static final int FIRE_BUTTON = 2;
    public static final int JUMP_BUTTON = 3;
    public static final int PAUSE_BUTTON = 4;

    /*COSTANTI DEGLI INDICI DEI BOTTONI DEI MENU DURANTE IL GIOCO NELL'ARRAY*/
    public static final int TRYAGAIN_BUTTON = 0;
    public static final int MAINMENU_BUTTON = 1;


    /*COSTANTI CHE DEFINISCONO LE DIMENSIONI DEI BOTTONI*/
    public static final int LEFT_BUTTON_X = 30;
    public static final int LEFT_BUTTON_Y = 260;
    public static final int LEFT_BUTTON_WIDTH = 50;
    public static final int LEFT_BUTTON_HEIGHT = 50;

    public static final int RIGHT_BUTTON_X = 100;
    public static final int RIGHT_BUTTON_Y = 260;
    public static final int RIGHT_BUTTON_WIDTH = 50;
    public static final int RIGHT_BUTTON_HEIGHT = 50;

    public static final int FIRE_BUTTON_X = 400;
    public static final int FIRE_BUTTON_Y = 260;
    public static final int FIRE_BUTTON_WIDTH = 50;
    public static final int FIRE_BUTTON_HEIGHT = 50;

    public static final int JUMP_BUTTON_X = 320;
    public static final int JUMP_BUTTON_Y = 260;
    public static final int JUMP_BUTTON_WIDTH = 50;
    public static final int JUMP_BUTTON_HEIGHT = 50;

    public static final int PAUSE_BUTTON_X = 440;
    public static final int PAUSE_BUTTON_Y = 10;
    public static final int PAUSE_BUTTON_WIDTH = 32;
    public static final int PAUSE_BUTTON_HEIGHT = 32;

    public static final int TRYAGAIN_BUTTON_X = 255;
    public static final int TRYAGAIN_BUTTON_Y = 200;
    public static final int TRYAGAIN_BUTTON_WIDTH = 90;
    public static final int TRYAGAIN_BUTTON_HEIGHT = 30;

    public static final int MAINMENU_BUTTON_X = 135;
    public static final int MAINMENU_BUTTON_Y = 200;
    public static final int MAINMENU_BUTTON_WIDTH = 90;
    public static final int MAINMENU_BUTTON_HEIGHT = 30;


    /*COSTANTI PER LA POSIZIONE DEL PUNTEGGIO*/
    public static final int SCORE_ICON_X = 210;
    public static final int SCORE_ICON_Y = 270;


    /*COSTANTI PER LA PERSISTENZA DELL'HIGHSCORE*/
    public static final String LEVEL1_HIGHSCORE ="level1";


}
